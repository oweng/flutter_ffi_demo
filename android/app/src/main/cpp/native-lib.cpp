#define ATTRIBUTES extern "C" __attribute__((visibility("default"))) __attribute__((used))
#include <opencv2\opencv.hpp>
#include <opencv2\core\mat.hpp>
#include <android_log.h>
#define DEBUG_NATIVE true

using namespace cv;

// decode 图片
ATTRIBUTES Mat *opencv_decodeImage(
    unsigned char *img,
    int32_t *imgLengthBytes)
{

    Mat *src = new Mat();
    std::vector<unsigned char> m;

    LOGD("opencv_decodeImage() ---  start imgLengthBytes:%d ",
         *imgLengthBytes);

    for (int32_t a = *imgLengthBytes; a >= 0; a--)
        m.push_back(*(img++));

    *src = imdecode(m, cv::IMREAD_COLOR);
    if (src->data == nullptr)
        return nullptr;

    if (DEBUG_NATIVE)
        LOGD(
            "opencv_decodeImage() ---  len before:%d  len after:%d  width:%d  height:%d",
            *imgLengthBytes, src->step[0] * src->rows,
            src->cols, src->rows);

    *imgLengthBytes = src->step[0] * src->rows;
    return src;
}

ATTRIBUTES
unsigned char *opencv_blur(
    uint8_t *imgMat,
    int32_t *imgLengthBytes,
    int32_t kernelSize)
{
    // 1. decode 图片
    Mat *src = opencv_decodeImage(imgMat, imgLengthBytes);
    if (src == nullptr || src->data == nullptr)
        return nullptr;
    if (DEBUG_NATIVE)
    {
        LOGD(
            "opencv_blur() ---  width:%d   height:%d",
            src->cols, src->rows);

        LOGD(
            "opencv_blur() ---  len:%d ",
            src->step[0] * src->rows);
    }

    // 2. 高斯模糊
    GaussianBlur(*src, *src, Size(kernelSize, kernelSize), 15, 0, 4);
    std::vector<uchar> buf(1); // imencode() will resize it
                               //    Encoding with b       mp : 20-40ms
                               //    Encoding with jpg : 50-70 ms
                               //    Encoding with png: 200-250ms
    // 3. encode 图片
    imencode(".png", *src, buf);

    if (DEBUG_NATIVE)
    {
        LOGD(
            "opencv_blur()  resulting image  length:%d %d x %d", buf.size(),
            src->cols, src->rows);
    }

    *imgLengthBytes = buf.size();

    // the return value may be freed by GC before dart receive it??
    // Sometimes in Dart, ImgProc.computeSync() receives all zeros while here buf.data() is filled correctly
    // Returning a new allocated memory.
    // Note: remember to free() the Pointer<> in Dart!

    // 3. 返回data
    return buf.data();
}
