# flutter ffi demo

#### 介绍
代码参考自
1. [如何在 Flutter 中调用 C++ 代码](https://juejin.cn/post/6976824832595853342)
2. [Android NDK 打印日志](https://juejin.cn/post/6910460306646433806)
3. [配置 CMake](https://developer.android.google.cn/studio/projects/configure-cmake?hl=zh-cn#kotlin)

使用方法，下载opencv4.8的android sdk，按照下图结构把相关文件复制到指定位置，具体可以参考[如何在 Flutter 中调用 C++ 代码](https://juejin.cn/post/6976824832595853342)
![项目结构](android%E9%A1%B9%E7%9B%AE%E7%BB%93%E6%9E%84.png)